export const getAllOrders = () => (dispatch) => {
  fetch("http://localhost:8080/mall/order")
    .then(response => response.json())
    .then(response => {
      dispatch({
        type: 'GET_ALL_ORDERS',
        orders: {
          response
        }
      });
    })
};