const initState = {
  orders: {
    response: []
  }
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_ALL_ORDERS':
      return{
        ...state,
        orders: action.orders
      };
    default:
      return state;
  }
}