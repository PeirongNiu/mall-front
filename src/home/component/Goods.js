import React, {Component} from 'react';
class Goods extends Component{
  constructor(props, context) {
    super(props, context);
    this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd(){
    fetch('http://localhost:8080/mall/order',
      {
        method: 'post',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          goodsId: this.props.goodsId,
          num: 1
        })
      })
  }

  render() {
    return (
      <div>
        <section>
          <img/>
          <label>{this.props.name}</label>
          <span>单价:{this.props.price}元/{this.props.unit}</span>
        </section>
        <button onClick={this.handleAdd}>+</button>
      </div>
    )
  }
}

export default Goods;