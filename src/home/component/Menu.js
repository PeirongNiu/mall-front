import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class Menu extends Component{
  render() {
    return(
      <section>
        <NavLink to="/">商城</NavLink>
        <NavLink to="/order">订单</NavLink>
        <NavLink to="/create">添加商品</NavLink>
      </section>
    )
  }
}

export default Menu