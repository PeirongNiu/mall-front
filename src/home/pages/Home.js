import React, {Component} from 'react';
import {getAllGoods} from "../actions/GetAllGoodsAction";
import Goods from "../component/Goods";
import Menu from "../component/Menu";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

class Home extends Component {

  componentDidMount() {
    this.props.getAllGoods()
  }


  render() {
    const goods = this.props.goods
    return (
      <div>
        <Menu/>
        <section>
          {goods.response.map(value => {
            return <Goods goodsId={value.goodsId} name={value.name} price={value.price} unit={value.unit} img={value.img}/>
          })}
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  goods: state.GetAllGoodsReducer.goods
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllGoods
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);