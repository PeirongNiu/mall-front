import React, {Component} from 'react';
import {connect} from "react-redux";
import {getAllGoods} from "../actions/GetAllGoodsAction";
import {getAllOrders} from "../actions/GetAllOrdersAction";
import Order from "../component/Order";
import {bindActionCreators} from "redux";
import Menu from "../component/Menu";

class ShowOrder extends Component{
  componentDidMount() {
    this.props.getAllOrders()
  }

  render() {
    const order = this.props.orders.response;
    return(
      <section>
        <Menu/>
        <ul>
          <li>
            <span>名字</span>
            <span>单价</span>
            <span>数量</span>
            <span>单位</span>
            <span>操作</span>
          </li>
        </ul>
        <ul>
          <div>
            {order.map(value=>{
              return <Order name={value.name} price={value.price} num={value.num} unit={value.unit}/>
            })}
          </div>
        </ul>
      </section>
    )
  }
}
const mapStateToProps = state => ({
  orders: state.GetAllOrdersReducer.orders
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllOrders
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShowOrder)