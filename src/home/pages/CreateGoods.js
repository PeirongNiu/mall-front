import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import Menu from "../component/Menu";

class createGoods extends Component {

  constructor(props, context) {
    super(props, context);

    this.state = {
      name: '',
      price: '',
      unit: '',
      img: '',
      flag: true
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handlePrice = this.handlePrice.bind(this);
    this.handleUnit = this.handleUnit.bind(this);
    this.handleImg = this.handleImg.bind(this);
  }


  handleName(e) {
    this.setState({
      name: e.target.value
    });
    if (this.state.name == '' || this.state.price == '' || this.state.unit == '' || this.state.img == '')
      this.setState({
        flag: false
      })
  }

  handlePrice(e){
    this.setState({
      price: e.target.value
    });
    if (this.state.name == '' || this.state.price == '' || this.state.unit == '' || this.state.img == '')
      this.setState({
        flag: false
      })
  }

  handleUnit(e){
    this.setState({
      unit: e.target.value
    });
    if (this.state.name == '' || this.state.price == '' || this.state.unit == '' || this.state.img == '')
      this.setState({
        flag: false
      })
  }

  handleImg(e){
    this.setState({
      img: e.target.value
    });
    if (this.state.name == '' || this.state.price == '' || this.state.unit == '' || this.state.img == '')
      this.setState({
        flag: false
      })
  }

  handleSubmit() {
    fetch('http://localhost:8080/mall/goods',
      {
      method: 'post',
        headers: {
        'Content-Type': 'application/json'
      },
        body: JSON.stringify({
          name: this.state.name,
          price: this.state.price,
          unit: this.state.unit,
          img: this.state.img
        })
      })
  }

  render() {
    return (
      <section>
        <Menu/>
        <h3>添加商品</h3>
        <div>
          <span>名称</span>
          <input onChange={this.handleName}/>
        </div>
        <div>
          <span>价格</span>
          <input onChange={this.handlePrice}/>
        </div>
        <div>
          <span >单位</span>
          <input onChange={this.handleUnit}/>
        </div>
        <div>
          <span >图片</span>
          <input onChange={this.handleImg}/>
        </div>
        <NavLink to="/" onClick={this.handleSubmit}>
          <button type="submit" disabled={this.state.flag} >提交</button>
        </NavLink>
      </section>
    );
  }

}

export default createGoods;