import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import CreateGoods from "./home/pages/CreateGoods";
import Home from "./home/pages/Home";
import ShowOrder from "./home/pages/ShowOrder";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route path="/create" component={CreateGoods}></Route>
            <Route path="/order" component={ShowOrder}></Route>
            <Route path='/' component={Home}></Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;