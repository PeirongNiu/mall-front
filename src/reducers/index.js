import {combineReducers} from "redux";
import GetAllGoodsReducer from "../home/reducers/GetAllGoodsReducer";
import GetAllOrdersReducer from "../home/reducers/GetAllOrdersReducer";

const reducers = combineReducers({
  GetAllGoodsReducer,
  GetAllOrdersReducer
});
export default reducers;